FROM python:2.7 as build

COPY requirements.txt /source/
RUN set -x \
    && cd /source \
    && pip install -r requirements.txt
COPY . /source/
ARG config=site
WORKDIR /source
RUN set -x \
    && hyde gen -d public -c ${config}.yaml

FROM nginx:alpine as deploy
COPY --from=build /source/public /usr/share/nginx/html
COPY --from=build /source/nginx.conf /etc/nginx
